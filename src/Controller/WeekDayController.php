<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class WeekDayController extends AbstractController
{
    /**
     * @Route("/week/day", name="week_day")
     */
    public function index()
    {
        return $this->render('week_day/index.html.twig', [
            'controller_name' => 'WeekDayController',
        ]);
    }
}
