<?php

namespace App\Entity;

use App\Repository\WeekDayRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WeekDayRepository::class)
 */
class WeekDay
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $studyHours;

    /**
     * @ORM\ManyToMany(targetEntity=Subject::class, inversedBy="weekDays")
     */
    private $subjects;

    public function __construct()
    {
        $this->subjects = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStudyHours(): ?int
    {
        return $this->studyHours;
    }

    public function setStudyHours(int $studyHours): self
    {
        $this->studyHours = $studyHours;

        return $this;
    }

    /**
     * @return Collection|Subject[]
     */
    public function getSubjects(): Collection
    {
        return $this->subjects;
    }

    public function addSubject(Subject $subject): self
    {
        if (!$this->subjects->contains($subject)) {
            $this->subjects[] = $subject;
            $subject->addWeekDay($this);
        }

        return $this;
    }

    public function removeSubject(Subject $subject): self
    {
        if ($this->subjects->contains($subject)) {
            $this->subjects->removeElement($subject);
        }

        return $this;
    }

    public function fromJson(array $json){
        $this->setStudyHours($json['studyHours']);
        foreach ($json['subjects'] as $jsonSub){
            $subject = new Subject();
            $this->addSubject($subject->fromJson($jsonSub));
        }
        return $this;
    }
}
