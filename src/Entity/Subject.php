<?php

namespace App\Entity;

use App\Repository\SubjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SubjectRepository::class)
 */
class Subject
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $difficulty;

    /**
     * @ORM\ManyToMany(targetEntity=WeekDay::class, mappedBy="subjects")
     */
    private $weekDays;

    /**
     * @ORM\OneToMany(targetEntity=Workload::class, mappedBy="subject")
     */
    private $workloads;

    public function __construct()
    {
        $this->weekDays = new ArrayCollection();
        $this->workloads = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDifficulty(): ?string
    {
        return $this->difficulty;
    }

    public function setDifficulty(string $difficulty): self
    {
        $this->difficulty = $difficulty;

        return $this;
    }

    /**
     * @return Collection|WeekDay[]
     */
    public function getWeekDays(): Collection
    {
        return $this->weekDays;
    }

    public function addWeekDay(WeekDay $weekDay): self
    {
        if (!$this->weekDays->contains($weekDay)) {
            $this->weekDays[] = $weekDay;
        }

        return $this;
    }

    public function removeWeekDay(WeekDay $weekDay): self
    {
        if ($this->weekDays->contains($weekDay)) {
            $this->weekDays->removeElement($weekDay);
            $weekDay->removeSubject($this);
        }

        return $this;
    }

    /**
     * @return Collection|Workload[]
     */
    public function getWorkloads(): Collection
    {
        return $this->workloads;
    }

    public function addWorkload(Workload $workload): self
    {
        if (!$this->workloads->contains($workload)) {
            $this->workloads[] = $workload;
            $workload->setSubject($this);
        }

        return $this;
    }

    public function removeWorkload(Workload $workload): self
    {
        if ($this->workloads->contains($workload)) {
            $this->workloads->removeElement($workload);
            // set the owning side to null (unless already changed)
            if ($workload->getSubject() === $this) {
                $workload->setSubject(null);
            }
        }

        return $this;
    }

    public function toJson(Subject $subject): array{

        $json['id'] = $subject->getId();
        $json['name'] = $subject->getName();
        $json['difficulty'] = $subject->getDifficulty();
        $json['workloads'] =  $subject->getWorkloads();
        $json['weekdays'] = $subject->getWeekDays();

        return $json;
    }

    public function fromJson(array $subject): self {

        $this->setName($subject['name'])
             ->setDifficulty($subject['difficulty']);

        return $this;
    }
}
