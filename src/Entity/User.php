<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $studyLevel;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $educationCenter;

    /**
     * @ORM\OneToMany(targetEntity=Workload::class, mappedBy="user", orphanRemoval=true)
     */
    private $workloads;

    public function __construct()
    {
        $this->workloads = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getStudyLevel(): ?string
    {
        return $this->studyLevel;
    }

    public function setStudyLevel(?string $studyLevel): self
    {
        $this->studyLevel = $studyLevel;

        return $this;
    }

    public function getEducationCenter(): ?string
    {
        return $this->educationCenter;
    }

    public function setEducationCenter(string $educationCenter): self
    {
        $this->educationCenter = $educationCenter;

        return $this;
    }

    /**
     * @return Collection|Workload[]
     */
    public function getWorkloads(): Collection
    {
        return $this->workloads;
    }

    public function addWorkload(Workload $workload): self
    {
        if (!$this->workloads->contains($workload)) {
            $this->workloads[] = $workload;
            $workload->setUser($this);
        }

        return $this;
    }

    public function removeWorkload(Workload $workload): self
    {
        if ($this->workloads->contains($workload)) {
            $this->workloads->removeElement($workload);
            // set the owning side to null (unless already changed)
            if ($workload->getUser() === $this) {
                $workload->setUser(null);
            }
        }

        return $this;
    }
}
