<?php

namespace App\Entity;

use App\Repository\WeekRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WeekRepository::class)
 */
class Week
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=WeekDay::class, cascade={"persist", "remove"})
     */
    private $monday;

    /**
     * @ORM\OneToOne(targetEntity=WeekDay::class, cascade={"persist", "remove"})
     */
    private $tuesday;

    /**
     * @ORM\OneToOne(targetEntity=WeekDay::class, cascade={"persist", "remove"})
     */
    private $thursday;

    /**
     * @ORM\OneToOne(targetEntity=WeekDay::class, cascade={"persist", "remove"})
     */
    private $wednesday;

    /**
     * @ORM\OneToOne(targetEntity=WeekDay::class, cascade={"persist", "remove"})
     */
    private $friday;

    /**
     * @ORM\OneToOne(targetEntity=WeekDay::class, cascade={"persist", "remove"})
     */
    private $saturday;

    /**
     * @ORM\OneToOne(targetEntity=WeekDay::class, cascade={"persist", "remove"})
     */
    private $sunday;

    public function __construct(){
        $this->monday = new WeekDay();
        $this->tuesday = new WeekDay();
        $this->wednesday = new WeekDay();
        $this->thursday = new WeekDay();
        $this->friday = new WeekDay();
        $this->saturday = new WeekDay();
        $this->sunday = new WeekDay();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMonday(): ?WeekDay
    {
        return $this->monday;
    }

    public function setMonday(WeekDay $monday): self
    {
        $this->monday = $monday;

        return $this;
    }

    public function getTuesday(): ?WeekDay
    {
        return $this->tuesday;
    }

    public function setTuesday(WeekDay $tuesday): self
    {
        $this->tuesday = $tuesday;

        return $this;
    }

    public function getWednesday(): ?string
    {
        return $this->wednesday;
    }

    public function setWednesday(string $wednesday): self
    {
        $this->wednesday = $wednesday;

        return $this;
    }

    public function getThursday(): ?WeekDay
    {
        return $this->thursday;
    }

    public function setThursday(WeekDay $thursday): self
    {
        $this->thursday = $thursday;

        return $this;
    }

    public function getFriday(): ?WeekDay
    {
        return $this->friday;
    }

    public function setFriday(WeekDay $friday): self
    {
        $this->friday = $friday;

        return $this;
    }

    public function getSaturday(): ?WeekDay
    {
        return $this->saturday;
    }

    public function setSaturday(WeekDay $saturday): self
    {
        $this->saturday = $saturday;

        return $this;
    }

    public function getSunday(): ?WeekDay
    {
        return $this->sunday;
    }

    public function setSunday(?WeekDay $sunday): self
    {
        $this->sunday = $sunday;

        return $this;
    }

    public function fromJson(array $json){

        $this->setMonday($this->monday->fromJson($json['monday']))
             ->setTuesday($this->tuesday->fromJson($json['tuesday']))
             ->setWednesday($this->wednesday->fromJson($json['wednesday']))
             ->setThursday($this->thursday->fromJson($json['thursday']))
             ->setFriday($this->friday->fromJson($json['friday']))
             ->setSaturday($this->saturday->fromJson($json['saturday']))
             ->setSunday($this->sunday->fromJson($json['sunday']));
    }
}
